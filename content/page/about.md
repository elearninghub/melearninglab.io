---
title: Machine eLearning
subtitle: Behind the Scenes
comments: false
---

# Machine Learning Algorithms

- [Learning Algorithms](http://www.cis.upenn.edu/~danielkh/learn.html)

# Convex Optimization

## Deep learning
- [Convex Neural Networks](https://www.iro.umontreal.ca/~lisa/pointeurs/convex_nnet_nips2005.pdf)
- [When Are Nonconvex Problems Not Scary?](https://arxiv.org/abs/1510.06096)

